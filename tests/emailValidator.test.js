/* eslint-env mocha */
import { expect } from 'chai';
import {
  validate, validateAsync,
  validateWithThrow, validateWithLog,
} from '../js/email-validator';

describe('validate', () => {
  it('should return true for valid email endings', () => {
    expect(validate('test@gmail.com')).to.equal(true);
    expect(validate('test@outlook.com')).to.equal(true);
    expect(validate('test@yandex.ru')).to.equal(true);
  });

  it('should return false for invalid email endings', () => {
    expect(validate('test@yahoo.com')).to.equal(false);
    expect(validate('test@mail.ru')).to.equal(false);
    expect(validate('test@test.com')).to.equal(false);
  });
});

describe('validateAsync', () => {
  it('should return true for valid email endings', async () => {
    expect(await validateAsync('test@gmail.com')).to.equal(true);
    expect(await validateAsync('test@outlook.com')).to.equal(true);
    expect(await validateAsync('test@yandex.ru')).to.equal(true);
  });

  it('should return false for invalid email endings', async () => {
    expect(await validateAsync('test@yahoo.com')).to.equal(false);
    expect(await validateAsync('test@mail.ru')).to.equal(false);
    expect(await validateAsync('test@test.com')).to.equal(false);
  });
});

describe('validateWithThrow', () => {
  it('should return true for valid email endings', () => {
    expect(validateWithThrow('test@gmail.com')).to.equal(true);
    expect(validateWithThrow('test@outlook.com')).to.equal(true);
    expect(validateWithThrow('test@yandex.ru')).to.equal(true);
  });

  it('should throw an error for invalid email endings', () => {
    expect(() => validateWithThrow('test@yahoo.com')).to.throw('Invalid email');
    expect(() => validateWithThrow('test@mail.ru')).to.throw('Invalid email');
    expect(() => validateWithThrow('test@test.com')).to.throw('Invalid email');
  });
});

describe('validateWithLog', () => {
  let originalLog;
  let consoleOutput;

  beforeEach(() => {
    // eslint-disable-next-line no-console
    originalLog = console.log;
    consoleOutput = [];
    // eslint-disable-next-line no-console
    console.log = (message) => consoleOutput.push(message);
  });

  it('should return true for valid email endings and log the result', () => {
    expect(validateWithLog('test@gmail.com')).to.equal(true);
    expect(consoleOutput).to.include(true);
  });

  it('should return false for invalid email endings and log the result', () => {
    expect(validateWithLog('test@test.com')).to.equal(false);
    expect(consoleOutput).to.include(false);
  });

  afterEach(() => {
    // eslint-disable-next-line no-console
    console.log = originalLog;
  });
});
