const path = require('path');
const webpack = require('webpack');
const proxy = require('http-proxy-middleware');

const TerserWebpackPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production';

  return {
    mode: isProduction ? 'production' : 'development',
    entry: './js/main.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
      clean: true,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            },
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './index.html',
      }),
      new CopyWebpackPlugin({
        patterns: [{ from: 'assets/*logo*', to: './' }],
      }),
      new webpack.HotModuleReplacementPlugin(),
    ],
    optimization: {
      minimize: isProduction,
      minimizer: [new TerserWebpackPlugin()],
    },
    devServer: {
      static: {
        directory: path.join(__dirname, 'dist'),
      },
      proxy: [
        {
          context: ['/api'],
          target: 'http://localhost:3000',
          pathRewrite: { '^/api': '' },
        },
      ],
      port: 8080,
      hot: false,
      liveReload: true,
    },
  };
};
