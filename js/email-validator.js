const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export function validate(email) {
  return VALID_EMAIL_ENDINGS.some((ending) => email.endsWith(ending));
}

export function validateAsync(email) {
  return new Promise((resolve) => {
    resolve(validate(email));
  });
}

export function validateWithThrow(email) {
  if (validate(email)) {
    return true;
  }
  throw new Error('Invalid email');
}

export function validateWithLog(email) {
  const result = validate(email);
  // eslint-disable-next-line no-console
  console.log(result);
  return result;
}
