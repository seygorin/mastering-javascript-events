import { validate } from './email-validator';

const JoinUsSection = (() => {
  const containerHTML = () => `
  <div class="container">
    <h2 class="join-section__headline">Join Our Program</h2>
    <h3 class="join-section__subheadline">Sed do eiusmod tempor incididunt<br>ut labore et dolore magna aliqua.</h3>
    <form class="join-section__form" action="#">
      <input class="join-section__form-input" type="email" placeholder="Email" required>
      <button class="join-section__form-button" type="submit">Subscribe</button>
      <div class="join-section__error-message"></div>
    </form>
  </div>
`;

  const updateSubscriptionState = (emailInput, submitButton, isSubscribed) => {
    if (isSubscribed) {
      localStorage.setItem('subsEmail', emailInput.value);
    } else {
      localStorage.removeItem('subsEmail');
    }
    emailInput.classList.toggle('join-section__input-hidden', isSubscribed);
    const button = submitButton;
    button.textContent = isSubscribed ? 'Unsubscribe' : 'Subscribe';
  };

  const subscribe = async (email) => fetch('/api/subscribe', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email }),
  });

  const unsubscribe = async (email) => fetch('/api/unsubscribe', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email }),
  });

  const setButtonState = (submitButton, disabled) => {
    submitButton.disabled = disabled; // eslint-disable-line no-param-reassign
    submitButton.style.opacity = disabled ? '0.5' : '1'; // eslint-disable-line no-param-reassign
  };

  const handleSubscriptionError = async (response) => {
    if (response.status === 422) {
      const data = await response.json();
      if (data.error) {
        window.alert(data.error); // eslint-disable-line no-alert
      }
    } else {
      throw new Error('Subscription failed');
    }
  };

  const showErrorMessage = (errorMessage, message) => {
    errorMessage.textContent = message; // eslint-disable-line no-param-reassign
    errorMessage.style.display = 'block'; // eslint-disable-line no-param-reassign
  };

  const hideErrorMessage = (errorMessage) => {
    errorMessage.style.display = 'none'; // eslint-disable-line no-param-reassign
  };

  const handleSubscription = async (emailInput, submitButton, errorMessage) => {
    const isSubscribed = submitButton.textContent === 'Unsubscribe';
    const isValidEmail = validate(emailInput.value);
    const requestInProgress = submitButton.disabled;

    if (requestInProgress) {
      return;
    }

    if (!isValidEmail) {
      showErrorMessage(errorMessage, 'Please enter a valid email address.');
      return;
    }
    setButtonState(submitButton, true);

    try {
      let response;
      if (isSubscribed) {
        response = await unsubscribe(emailInput.value);
      } else {
        response = await subscribe(emailInput.value);
      }

      if (response.ok) {
        updateSubscriptionState(emailInput, submitButton, !isSubscribed);
        localStorage.setItem('isSubscribed', String(!isSubscribed));
        hideErrorMessage(errorMessage);
      } else {
        await handleSubscriptionError(response);
      }
    } catch (error) {
      showErrorMessage(errorMessage, error.message);
    } finally {
      setButtonState(submitButton, false);
    }
  };

  const initializeForm = (emailInput, submitButton) => {
    const savedEmail = localStorage.getItem('subsEmail');
    const isSubscribed = localStorage.getItem('isSubscribed') === 'true';
    emailInput.value = savedEmail || ''; // eslint-disable-line no-param-reassign
    if (isSubscribed) {
      updateSubscriptionState(emailInput, submitButton, true);
    }
  };

  const createSection = () => {
    const section = document.createElement('section');
    section.classList.add('join-section');
    section.innerHTML = containerHTML();

    const form = section.querySelector('.join-section__form');
    const emailInput = form.querySelector('.join-section__form-input');
    const submitButton = form.querySelector('.join-section__form-button');
    const errorMessage = section.querySelector('.join-section__error-message');

    initializeForm(emailInput, submitButton);

    form.addEventListener('submit', (event) => {
      event.preventDefault();
      handleSubscription(emailInput, submitButton, errorMessage);
    });

    return section;
  };

  return {
    create: createSection,
  };
})();

export default JoinUsSection;
