import JoinUsSection from './join-us-section';

function StandardProgram() {
  this.create = function () {
    return JoinUsSection.create();
  };
}

function AdvancedProgram() {
  this.create = function () {
    const section = JoinUsSection.create();

    const title = section.querySelector('.join-section__headline');
    const subscribeButton = section.querySelector('.join-section__form-button');

    if (title && subscribeButton) {
      title.textContent = 'Join Our Advanced Program';
      subscribeButton.textContent = 'Subscribe to Advanced Program';
    }

    return section;
  };
}

const SectionCreator = {
  create(type) {
    switch (type) {
    case 'standard':
      return new StandardProgram().create();
    case 'advanced':
      return new AdvancedProgram().create();
    default:
      throw new Error('Invalid type');
    }
  },
};

export default SectionCreator;
