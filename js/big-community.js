import { LoremIpsum } from 'lorem-ipsum';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 3,
    min: 2,
  },
  wordsPerSentence: {
    max: 6,
    min: 3,
  },
});

const BigCommunity = (() => {
  const containerHTML = () => `
  <div class="smaller-container">
    <h2 class="big-community__headline">Big Community of <br/> People Like You </h2>
    <h3 class="big-community__subheadline">We’re proud of our products, and we’re really excited <br/> when we get feedback from our users.</h3>
    <div class="big-community__user-card-container"></div>
  </div>
`;

  const createUserCard = (name, position, avatar) => `
  <div class="big-community__user-card">
    <div class="big-community__user-card-inner">
      <img src="${avatar}" alt="${name}'s Avatar" class="big-community__user-card-avatar">
      <div class="big-community__user-card-info">
      <p class="big-community__user-card-description">${lorem.generateParagraphs(1)}</p>
      <h4 class="big-community__user-card-name">${name.toUpperCase()}</h4>
        <p class="big-community__user-card-position">${position}</p>
      </div>
    </div>
  </div>
  `;

  const createSkeletonCard = () => `
  <div class="big-community__user-card">
    <div class="big-community__user-card-inner">
      <div class="big-community__user-card-avatar skeleton"></div>
      <div class="big-community__user-card-info ">
        <div class="big-community__user-card-description skeleton"></div>
        <div class="big-community__user-card-name skeleton"></div>
        <div class="big-community__user-card-position skeleton"></div>
      </div>
    </div>
  </div>
`;

  const renderSkeletons = (count, container) => {
    for (let i = 0; i < count; i += 1) {
      const skeletonCardHTML = createSkeletonCard();
      container.insertAdjacentHTML('beforeend', skeletonCardHTML);
    }
  };

  const fetchUsers = async () => {
    const response = await fetch('/api/community');
    return response.json();
  };

  const renderUsers = (users, container) => {
    users.forEach((user) => {
      const userCardHTML = createUserCard(
        `${user.firstName} ${user.lastName}`,
        user.position,
        user.avatar,
      );
      container.insertAdjacentHTML('beforeend', userCardHTML);
    });
  };

  const createSection = async () => {
    const section = document.createElement('section');
    section.classList.add('big-community');
    section.innerHTML = containerHTML();

    const userCardContainer = section.querySelector('.big-community__user-card-container');

    renderSkeletons(3, userCardContainer);

    try {
      const users = await fetchUsers();
      userCardContainer.innerHTML = '';
      renderUsers(users, userCardContainer);
    } catch (error) {
      console.error('Error fetching data:', error); // eslint-disable-line no-console
    }

    return section;
  };
  return {
    create: createSection,
  };
})();

export default BigCommunity;
