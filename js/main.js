import SectionCreator from './section-creator';
import BigCommunity from './big-community';
import '../css/normalize.css';
import '../css/styles.css';

document.addEventListener('DOMContentLoaded', async () => {
  const main = document.querySelector('main');
  const mainSection = main.querySelector('.main-section');

  const bigCommunitySection = await BigCommunity.create();
  mainSection.after(bigCommunitySection);

  const joinUsSection = SectionCreator.create('standard');
  main.appendChild(joinUsSection);
});
