# Mastering-JavaScript-Events-Patterns

To get started, let's practice working with JavaScript events that allow users to interact with the content on web pages. <br/>
Visual presentation on [Figma Project](https://www.figma.com/file/MnzJnLZtw6FBVcYQD8SQbx/JavaScript-advanced?type=design&node-id=0-1&mode=design&t=aJrGPRijgzLk0mIR-0).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Task

The things you need before installing the software.

- Install any IDE or use the one that is already installed (for example, Visual Studio Code)
- Get your account in a public GitLab repository and create a project named "mastering-javascript-events"
- Create a local folder for the future project or clone your earlier project "Prev-Task", or copy files from it

### Installation

A step by step guide that will tell you how to get the development environment up and running.

```
git clone https://gitlab.com/seygorin/mastering-javascript-events.git
```
